# -- coding: utf-8 --

from subprocess import call
from os import curdir
from os.path import abspath, exists, isdir, join
from posix import getpid

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

working_directory = abspath(curdir)
lock_name = join(working_directory, 'super_hacker_program.lock')
thread_lock_name = join(lock_name, 'thread.lock')
pid_filename = join(lock_name, 'pid')
glade_file = join(working_directory, 'ui.glade')
show_keystr = "<Ctrl><Alt>S"
hide_keystr = "<Ctrl><Alt>H"
take_photo_keystr = "F"
SIGNAL = 'thread-exit'


def unlock():
    return call(['rm', '-rf', lock_name])


def lock():
    return call(['mkdir', lock_name])


def locked():
    return exists(lock_name) and isdir(lock_name)


def close():
    unlock()
    Gtk.main_quit()


def save_pid():
    with open(pid_filename, 'w') as f:
        f.write(str(getpid()))


def get_pid():
    with open(pid_filename) as f:
        return int(f.read())
