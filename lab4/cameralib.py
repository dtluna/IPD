from subprocess import call
from time import sleep
from datetime import datetime
from os import remove
from SimpleCV import Camera, VideoStream, Display
from threading import Thread, Lock
from pydispatch import dispatcher
from util import SIGNAL
import pygame


camera = Camera(0, threaded=False)
stop_lock = Lock()
camera_being_used = Lock()


def capture_image():
    camera_being_used.acquire()
    img = camera.getImage()
    camera_being_used.release()
    return img


def ramp_camera(camera):
    ramp_frames = 6
    for _ in range(ramp_frames):
        capture_image()


ramp_camera(camera)


def run_show_video():
    Thread(target=show_video).start()


def show_video():
    dis = Display()
    try:
        while not dis.isDone():
            capture_image().save(dis)
            sleep(1/25.0)
    except pygame.error:
        return


def write_photo(video_len):
    img = capture_image()
    image_name = '{}.jpg'.format(get_current_time())
    img.save(image_name)


def write_video(video_len):
    buffer_video_name = '{}.avi'.format(get_buffer_name())
    output_file_name = '{}.mp4'.format(get_current_time())

    vs = VideoStream(filename=buffer_video_name)
    delay = 1./25

    current_video_len = 0
    while current_video_len < video_len:
        if stop_lock.locked():
            break
        img = capture_image()
        sleep(delay)
        vs.writeFrame(img)
        current_video_len = vs.framecount/25

    convert_to_mp4(buffer_video_name, output_file_name)
    remove(buffer_video_name)


tasks = {'photo': write_photo, 'video': write_video}


def loop(loop_len, iter_count, func, video_len=None):
    for _ in range(iter_count):
        if stop_lock.locked():
            break
        func(video_len)
        if stop_lock.locked():
            break
        sleep(loop_len)

    dispatcher.send(signal=SIGNAL, sender=None)


def run_loop(loop_len, iter_count, task, video_len=None):
    if stop_lock.locked():
        stop_lock.release()
    Thread(target=loop,
           args=(loop_len, iter_count, tasks[task], video_len)).start()


def stop_loop():
    global stop_lock
    if not stop_lock.locked():
        stop_lock.acquire()


def get_current_time():
    now = datetime.now()
    date = '{}.{}.{}__{}:{}:{}.{}'.format(now.day, now.month,
                                          now.year, now.hour,
                                          now.minute, now.second,
                                          now.microsecond)
    return date


def get_buffer_name():
    now = datetime.now()
    return 'buffer{}'.format(now.second)


def convert_to_mp4(buffer_name, out_name):
    call_string = 'ffmpeg -i {0} {1}'.format(buffer_name, out_name)
    print(call_string)
    call(call_string.split())


if __name__ == '__main__':
    # TEST
    run_loop(loop_len=2, iter_count=15, task='photo')
    sleep(9)
    stop_loop()

    run_loop(loop_len=10, iter_count=15, task='video', video_len=5)
    sleep(2)
    stop_loop()

    sleep(10)
