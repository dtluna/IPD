# -- coding: utf-8 --
from util import close, glade_file, show_keystr, hide_keystr, take_photo_keystr, SIGNAL
from cameralib import run_loop, stop_loop, run_show_video, write_photo
from pydispatch import dispatcher
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Keybinder', '3.0')
from gi.repository import Gtk, Keybinder


tasks = {False: 'photo', True: 'video'}
hint_text = 'Press {} to hide and {} to show the window.'
idle_status_message = 'gibe de c0mm@nd pls b0ss'


class GUI(object):
    instanced = False
    builder = Gtk.Builder()

    window = None
    photo_button = None
    video_button = None
    start_button = None
    stop_button = None
    hide_button = None
    show_video_button = None

    iter_spinbutton = None
    loop_spinbutton = None
    video_spinbutton = None

    statusbar = None
    hint_label = None

    loop_is_running = False

    def __init__(self):
        if GUI.instanced:
            raise RuntimeError("You cannot make two instances of GUI class!")

        super(GUI, self).__init__()

        self.builder.add_from_file(glade_file)
        self.builder.connect_signals(self)
        self.init_gui_elements()
        self.init_key_binding()
        dispatcher.connect(self.switch_start_button, signal=SIGNAL,
                           sender=dispatcher.Any)
        dispatcher.connect(self.reset_status_bar, signal=SIGNAL,
                           sender=dispatcher.Any)
        GUI.instanced = True

    def init_gui_elements(self):
        self.window = self.builder.get_object('window')
        self.photo_button = self.builder.get_object('photo_button')
        self.video_button = self.builder.get_object('video_button')
        self.start_button = self.builder.get_object('start_button')
        self.stop_button = self.builder.get_object('stop_button')
        self.iter_spinbutton = self.builder.get_object('iter_spinbutton')
        self.loop_spinbutton = self.builder.get_object('loop_spinbutton')
        self.video_spinbutton = self.builder.get_object('video_spinbutton')
        self.statusbar = self.builder.get_object('statusbar')
        self.hide_button = self.builder.get_object('hide_button')
        self.hint_label = self.builder.get_object('hint_label')
        self.show_video_button = self.builder.get_object('show_video_button')

        self.hint_label.set_text(hint_text.format(hide_keystr, show_keystr))
        self.reset_status_bar()

        self.photo_button.set_active(True)

        self.window.show_all()

    def init_key_binding(self):
        Keybinder.init()
        Keybinder.bind(show_keystr, self.show)
        Keybinder.bind(hide_keystr, self.hide)
        Keybinder.bind(take_photo_keystr, write_photo)

    def show(self, keystr=None):
        self.window.show_all()

    def hide(self, wigdet=None, data=None):
        self.window.hide()
        while Gtk.events_pending():
            Gtk.main_iteration()

    def run(self):
        Gtk.main()

    def switch_video_spinbutton(self, source, target=None):
        is_editable = self.video_spinbutton.get_editable()
        self.video_spinbutton.set_editable(not is_editable)

    def get_task(self):
        return tasks[self.video_button.get_active()]

    def reset_status_bar(self):
        context = self.statusbar.get_context_id(idle_status_message)
        self.statusbar.push(context, idle_status_message)

    def fill_status_bar(self, task):
        status_bar_msg = '{}s are being made.'.format(task.capitalize())
        context = self.statusbar.get_context_id(status_bar_msg)
        self.statusbar.push(context, status_bar_msg)

    def switch_start_button(self, widget=None, data=None):
        is_sensitive = self.start_button.get_sensitive()
        self.stop_button.set_sensitive(is_sensitive)
        self.start_button.set_sensitive(not is_sensitive)

    def start(self, widget=None, data=None):
        self.hide()
        self.loop_is_running = True
        loop_len = self.loop_spinbutton.get_value_as_int()
        iter_count = self.iter_spinbutton.get_value_as_int()
        task = self.get_task()
        self.fill_status_bar(task)

        video_length = self.video_spinbutton.get_value_as_int()
        run_loop(loop_len, iter_count, task, video_length)

    def stop(self, widget=None, data=None):
        stop_loop()
        self.reset_status_bar()

    def show_video(self, widget=None, data=None):
        self.show_video_button.set_sensitive(False)
        run_show_video()
        self.show_video_button.set_sensitive(True)

    def exit(self, widget, data=None):
        self.stop()
        close()
