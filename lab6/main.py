#!/usr/bin/env python3
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
import const
from pyudev import Context, Monitor, MonitorObserver
import dbus

const.PART_COLUMN = 0
const.LABEL_COLUMN = 1
const.FS_COLUMN = 2
const.UNMOUNT_COLUMN = 3
const.EJECT_COLUMN = 4
const.ui_file = 'ui.glade'
const.EVENT_MESSAGE = 'Action:{action} Partition:{partition} Label:"{label}" FS type:{fs_type}'

builder = Gtk.Builder()
builder.add_from_file(const.ui_file)

udev_context = Context()
monitor = Monitor.from_netlink(udev_context)
monitor.filter_by('block')


def is_usb(device):
    return device.get('ID_BUS', None) == 'usb'


def build(object_id: str):
    return builder.get_object(object_id)


def connect_signals(obj_or_map):
    builder.connect_signals(obj_or_map)


def get_device_name(partition):
    return partition[:-1]


def eject(button, partition):
    # tasty stuff
    bus = dbus.SystemBus()
    proxy = bus.get_object('org.freedesktop.UDisks',
                           '/org/freedesktop/UDisks')
    iface = dbus.Interface(proxy, 'org.freedesktop.UDisks')
    for dev in iface.EnumerateDevices():
        dev_obj = bus.get_object('org.freedesktop.UDisks', dev)
        dev_prop = dbus.Interface(dev_obj, 'org.freedesktop.DBus.Properties')
        if dev_prop.Get('', 'DeviceFile') == get_device_name(partition):
            idev = dbus.Interface(
                dev_obj, 'org.freedesktop.DBus.UDisks.Device')
            try:
                idev.get_dbus_method(
                    'DriveDetach',  # 'FileSystemUmount'
                    dbus_interface='org.freedesktop.UDisks.Device'
                )([])
            except dbus.exceptions.DBusException as e:
                dialog = Gtk.MessageDialog(
                    message_type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.CLOSE,
                    title="DBus error",
                    text=e.get_dbus_message()
                )
                dialog.run()
                dialog.destroy()
            else:
                return


def unmount(button, partition):
    bus = dbus.SystemBus()
    proxy = bus.get_object('org.freedesktop.UDisks',
                           '/org/freedesktop/UDisks')
    iface = dbus.Interface(proxy, 'org.freedesktop.UDisks')
    for dev in iface.EnumerateDevices():
        dev_obj = bus.get_object('org.freedesktop.UDisks', dev)
        dev_prop = dbus.Interface(dev_obj, 'org.freedesktop.DBus.Properties')
        if partition == dev_prop.Get('', 'DeviceFile'):
            idev = dbus.Interface(
                dev_obj, 'org.freedesktop.DBus.UDisks.Device')
            try:
                idev.get_dbus_method(
                    'FilesystemUnmount',  # 'FilesystemUmount'
                    dbus_interface='org.freedesktop.UDisks.Device'
                )([])
            except dbus.exceptions.DBusException as e:
                dialog = Gtk.MessageDialog(
                    message_type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.CLOSE,
                    title="DBus error",
                    text=e.get_dbus_message()
                )
                dialog.run()
                dialog.destroy()
            else:
                return


def add_row(self, partition, label, fs_type):
    def UnmountButton():
        button = Gtk.Button(label='Unmount')
        button.connect('clicked', unmount, partition)
        return button

    def EjectButton():
        button = Gtk.Button(label='Eject')
        button.connect('clicked', eject, partition)
        return button

    def Label(text='default text'):
        return Gtk.Label(text, selectable=True)

    def fill_column(child, position):
        self.attach(
            child=child,
            left=position,
            top=self.last_row,
            width=1,
            height=1
        )
    fill_column(Label(partition), const.PART_COLUMN)
    fill_column(Label(label), const.LABEL_COLUMN)
    fill_column(Label(fs_type), const.FS_COLUMN)
    fill_column(UnmountButton(), const.UNMOUNT_COLUMN)
    fill_column(EjectButton(), const.EJECT_COLUMN)
    self.last_row += 1
    self.show_all()


setattr(Gtk.Grid, 'add_row', add_row)
setattr(Gtk.Grid, 'last_row', 1)


class GUI(object):

    window = build('window')
    grid = build('grid')
    status_bar = build('status_bar')

    def __init__(self):
        super(GUI, self).__init__()
        self.fill_grid()
        self.window.show_all()
        GLib.timeout_add(interval=1000, function=self.reload_information)

    def reset_grid(self):
        while self.grid.last_row > 1:
            self.grid.remove_row(self.grid.last_row - 1)
            self.grid.last_row -= 1

    def fill_grid(self):
        self.reset_grid()
        # tasty stuff
        devs = udev_context.list_devices(subsystem='block', DEVTYPE='disk')
        devs = list(filter(is_usb, devs))
        for d in devs:
            for part in d.children:
                self.grid.add_row(
                    partition=part.device_node,
                    label=part.get('ID_FS_LABEL'),
                    fs_type=part.get('ID_FS_TYPE')
                )

    def reload_information(self):
        self.fill_grid()
        GLib.timeout_add_seconds(interval=1, function=self.reload_information)

    def handle_event(self, action, node):
        if node.get('DEVTYPE') == 'partition':
            self.set_status(
                message=const.EVENT_MESSAGE.format(
                    action=action,
                    partition=node.device_node,
                    label=node.get('ID_FS_LABEL'),
                    fs_type=node.get('ID_FS_TYPE')
                )
            )

    def set_status(self, message):
        context = self.status_bar.get_context_id(message)
        self.status_bar.push(context, message)

    def run(self):
        Gtk.main()

    def exit(self, widget=None):
        Gtk.main_quit()

if __name__ == '__main__':
    gui = GUI()
    connect_signals(gui)
    observer = MonitorObserver(monitor, gui.handle_event)
    observer.start()
    gui.run()
